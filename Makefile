.PHONY: all
all: rsi inf args

rsi: rsi.c
	gcc rsi.c  -o rsi

inf: inf.c
	gcc inf.c -o inf

args: args.c
	gcc args.c -o args

.PHONY: clean
clean:
	-rm -rf *.o *.exe
