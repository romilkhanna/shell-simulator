/* 
 * File:   rsi.c
 * Author: romil
 *
 * Created on January 11, 2013, 9:13 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

void parse(char*, char**);
int execute(char*, char**);
void changeDirectory(char*, char**);

/* 
 * This method will run forever asking the user
 * for more input commands. It takes the input
 * then parses it and then tries to execute it.
 */
int main(int argc, char** argv) {
    int lineMAX = 1024, argMAX = 15;
    char line[lineMAX];
    char *homedir;
    char *argList[argMAX];
    
    /* 
     * set the application working directory to the
     * users working directory
     */
    chdir("~");

    /* 
     *ask the user for more commands forever
     */
    while(true) {
        // set the prompt to the current working directory
        homedir = getcwd(NULL, 0);
        printf("RSI: %s > ", homedir);
        gets(line);
        parse(line, argList);
        execute(homedir, argList);
    }
    return EXIT_SUCCESS;
}

/*
 * This method takes the input line and tokenizes it
 * based on spaces. It stores the tokens in a list.
 */

void parse(char *line, char **argList) {
    char *tmp;
    int i = 0;
    tmp = strtok(line, " "); // get all tokens

    // run while there are tokens
    while(tmp) {
        argList[i] = tmp; // save each token
        tmp = strtok(NULL, " ");
        i++;
    }
    argList[i] = NULL; // mark the end
}

/*
 * This method executes the requested command from the user.
 * It takes the home directory and the arguement list as
 * parameters. The home directory is strictly passed in
 * if the user decides to change the working directory.
 * The arguement list is used to execute the command.
 */
int execute(char *homedir, char **argList) {
    // check exit
    if(strcmp(argList[0], "exit") == 0) {
        exit(EXIT_SUCCESS);
    }
    // check for change directory
    if(strcmp(argList[0], "cd") == 0) {
        if(argList[1] != NULL) {
            changeDirectory(homedir, argList);
        }
        return (EXIT_SUCCESS);
    }
    /*
     * Run the command in a child thread if not exit
     * or change directory.
     */
    pid_t child = fork();
    int status;
    if(child >= 0) {
        if(child == 0) { // child process
            int i;
            for(i = 0; argList[i] != NULL; i++);
            if(strcmp(argList[i-1], "&") == 0) {
                printf("child process pid is: %d", getpid());
                argList[i-1] = NULL;
            }
            execvp(argList[0], argList);
            exit(EXIT_FAILURE);
        } else { // parent process
            int i;
            for(i = 0; argList[i] != NULL; i++); // find last index
            /* 
             * Check if command was requested to be run in background
             * if so, don't wait for child and continue.
             */
            if(strcmp(argList[i-1], "&") == 0) {
                sleep(1);
                return EXIT_SUCCESS;
            }
            wait(&status); // wait for child

            // check if child failed to execute command
            if(status == 256) {
                printf("RSI: %s: command not found\n", argList[0]);
            }
            return EXIT_SUCCESS;
        }
    } else {
        printf("process failed to fork!");
        exit(EXIT_FAILURE);
    }
}

/*
 * This method is responsible for detecting what the
 * the user wants the current directory to be. It takes
 * the home directory and the argement list for parameters
 * the arguement list determines which action to take
 * and the home directory is what is changed.
 */
void changeDirectory(char *homedir, char **argList) {
    // go back a directory
    if(strcmp(argList[1], "..") == 0) {
        chdir("..");        
    } else if(strcmp(argList[1], "~") == 0) {
        chdir(getenv("HOME"));
    } else {
        chdir(argList[1]);
    }
}